import React, { Component } from 'react';
import { Viewer, rules } from '@sis-cc/dotstatsuite-components';
import { ChartsConfig } from '@sis-cc/dotstatsuite-visions'
import Share from './share';
import { get, isNaN, isNil, omit } from 'lodash';
import glamorous from 'glamorous';
import configLabels from '../labels';
import { appendTimelineTooltip, getShareData } from '../util';
import * as R from 'ramda';

const Container = glamorous.div({
  marginLeft: 10,
  marginRight: 10,
});

const ChartContainer = glamorous.div({
  marginBottom: 20,
});

const updateProperty = prop => R.pipe(
  R.chain(R.assoc('onSubmit'), R.prop('onChange')),
  R.dissoc('onChange')
)(prop);

class ChartWithConfig extends Component {
  shouldComponentUpdate = (nextProps) => nextProps.isDirty;

  render = () => {
    const {
      chartData,
      chartOptions,
      headerProps,
      footerProps,
      data,
      options,
      config,
      isNonIdealState,
      properties,
      dataflowId,
      type,
      errors,
      onChange,
      isFetching,
      sdmxUrl
    } = this.props;
    if (isFetching) {
      return null;
    }
    const { withLogo, withCopyright } = footerProps;

    const meta = {
      title: R.path(['title', 'label'], headerProps),
      subtitle: R.pipe(R.prop('subtitle'), R.pluck('label'))(headerProps),
      width: R.propOr(NaN, 'base.width', chartOptions),
      height: R.propOr(NaN, 'base.height', chartOptions),
    };

    const copyrightLabel = '©';
    const copyrightContent = <p>©OECD <a href="https://www.oecd.org/termsandconditions/">Terms & conditions</a></p>;

    const footer = {
      source: { label: R.prop('sourceLabel', footerProps), link: window.location.href },
      logo: withLogo ? config.logo : null,
      copyright: withCopyright ? { label: copyrightLabel, content: copyrightContent } : null
    };

    const shareData = getShareData({
      headerProps, footerProps: footer, chartData, chartOptions, type, properties, config, dataflowId, sdmxUrl, withCopyright
    });

    const header = {
      ...headerProps,
      children: <Share meta={meta} translations={{}} data={shareData} config={config} type={type}/>
    };

    const props = R.evolve({
      title: updateProperty,
      subtitle: updateProperty,
      source: updateProperty,
      width: updateProperty,
      height: updateProperty,
      minX: updateProperty,
      maxX: updateProperty,
      stepX: updateProperty,
      pivotX: updateProperty,
      minY: updateProperty,
      maxY: updateProperty,
      pivotY: updateProperty,
      stepY: updateProperty,
      freqStep: updateProperty,
    })(properties);

    let errorMessage = null;
    if (errors.noTime) {
      errorMessage = "A timeline chart cannot be shown because the data has no time dimension.";
    }

    return (
      <Container>
        <ChartContainer>
          <Viewer
            chartData={chartData}
            chartOptions={chartOptions}
            errorMessage={errorMessage}
            getAxisOptions={onChange}
            getResponsiveSize={onChange}
            headerProps={header}
            footerProps={footer}
            config={config}
            type={type}
          />
        </ChartContainer>
        <ChartsConfig
          labels={configLabels}
          properties={props}
        />
      </Container>
    );
  };
};

export default ChartWithConfig;
