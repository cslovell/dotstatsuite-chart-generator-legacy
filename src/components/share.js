import { get, has, isArray, isFunction } from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Classes, Dialog, Radio, RadioGroup, Spinner } from '@blueprintjs/core';
import glamorous from 'glamorous';
import Select from 'react-select';
import { stringify } from 'query-string';
import '@blueprintjs/core/dist/blueprint.css';
import ShareIcon from '../assets/share-icon.png';
import t4 from '../t4';
import 'react-select/dist/react-select.css';
import { refineShareData } from '../util';

const SUBTITLE_SEPARATOR = ' \u25cf ';

const GDialog = glamorous(Dialog)({
  borderRadius: 0,
  margin: '0 !important',
  padding: '0 !important',
  top: '10% !important',
  width: '80% !important',
  '& h2': {
    fontSize: 24,
    marginBottom: 20,
  },
  '& h3': {
    color: '#0b71b0',
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 20,
  },
  '& h4': {
    fontSize: 14,
    fontWeight: 'normal',
    marginBottom: 40,
  },
  '& textarea': {
    fontSize: 11,
    height: '120px !important',
  }
});

const DialogBody = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  margin: 0,
});

const ShareHeader = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between'
});

const ShareMain = glamorous.div({
  flexGrow: 3,
  padding: 20,
});

const ShareMedia = glamorous.div({
  marginBottom: 12,
  [`& ${Classes.BUTTON}`]: {
    margin: '0 10px 0 0'
  }
});

const ShareSide = glamorous.div({
  backgroundColor: '#deeaf1',
  padding: 20,
  width: '40%',
});

class Share extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      isSharing: false,
      hasFailed: false,
      shareMode: 'snapshot',
      permanentUri: '',
      hasFormatSelection: get(props.config, 'hasT4Formats', false),
      selectedFormat: 'default',
      sharedData: null,
      shareResponse: null,
    };
    this.onShare = this.onShare.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onChangeShareMode = this.onChangeShareMode.bind(this);
    this.onChangeFormat = this.onChangeFormat.bind(this);
    this.onOutput = this.onOutput.bind(this);
    this.configIsValid = (config) => has(config, 'read') && has(config, 'write');
  }

  onShare() {
    if (!this.configIsValid(this.props.config)) return;

    this.setState({ isFetching: true });
    const requestBody = {
      data: refineShareData(this.state.shareMode)(this.props.data),
      share: this.state.shareMode,
      type: this.props.type,
    };
    fetch(this.props.config.write, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(requestBody),
    }).then((res) => {
        this.setState({ isFetching: false });
        if (res.ok) {
          try {
            return res.json();
          } catch (err) {
            console.log(err);
            this.setState({ hasFailed: true });
          }
        } else {
          console.log(`${res.statusText} (${res.status})`);
          this.setState({ hasFailed: true });
        }
      }).then(responseJson => {
        const onShareGenerated = get(this.props, 'config.onShareGenerated');
        if (responseJson && isFunction(onShareGenerated)) {
          onShareGenerated({ responseJson, requestBody });
        }
        return responseJson;
      }).then((json) => {
        const permanentUri = `${this.props.config.read}?id=${json.id}`;

        // NaN is not falsy for get
        const _width = get(this.props, 'meta.width', NaN);
        const width = isNaN(_width) ? '100%' : _width;
        const _height = get(this.props, 'meta.height', NaN);
        const height = isNaN(_height) ? '100%' : _height;

        this.setState({
          permanentUri,
          embedCode:
`<iframe src="${permanentUri}" width="${width}" height="${height}" style="border: none;" allowfullscreen="true">
  <a href="${permanentUri}" rel="noopener noreferrer" target="_blank">${this.props.meta.title}</a>
</iframe>`,
          isSharing: true,
          selectedFormat: 'default',
          sharedData: requestBody,
          shareResponse: json,
        });
      });
  }

  onClose() {
    this.setState({ isSharing: false });
  }

  onChangeShareMode(event) {
    this.setState({ shareMode: event.target.value }, this.onShare);
  }

  onChangeFormat({ value }) {
    const width = get(t4, `values.${value}.width`, '100%');
    const height = get(t4, `values.${value}.height`, '100%');
    const embedCode =
`<iframe src="${this.state.permanentUri}" width="${width}" height="${height}" style="border: none;" allowfullscreen="true">
  <a rel="noopener noreferrer" href="${this.state.permanentUri}" target="_blank">${this.props.meta.title}</a>
</iframe>`;
    this.setState({ selectedFormat: value, embedCode })
  }

  onOutput(media) {
    let mediaHandler = null;
    switch (media) {
      case 'facebook':
        const uf = stringify({ u: this.state.permanentUri });
        mediaHandler = () => window.open(`https://www.facebook.com/sharer/sharer.php?${uf}`, '_blank');
        break;
      case 'twitter':
        const ut = stringify({ text: `${this.props.meta.title}\n${this.state.permanentUri}` })
        mediaHandler = () => window.open(`https://twitter.com/intent/tweet?${ut}`, '_blank');
        break;
      case 'email':
        const subject = encodeURIComponent(this.props.meta.title);
        const body = encodeURIComponent(`${this.props.meta.title}\n${this.state.permanentUri}`);
        mediaHandler = () => window.location.href = `mailto:?subject=${subject}&body=${body}`;
        break;
      case 'preview':
        //https://developer.mozilla.org/fr/docs/Web/API/Window/open
        mediaHandler = () => window.open(
          this.state.permanentUri,
          'preview',
          `resizable=yes,scrollbars=yes,status=yes`
        );
    }

    return () => {
      if (isFunction(mediaHandler)) {
        const onMediaShare = get(this.props, 'config.onMediaShare');
        if (isFunction(onMediaShare)) {
          const { sharedData, shareResponse } = this.state;
          onMediaShare({ media, sharedData, shareResponse });
        }
        mediaHandler();
      }
    };
  }

  render() {
    if (!this.configIsValid(this.props.config)) return null;

    const spinner = <Spinner className={Classes.SMALL} />;
    const permanentUrl = this.state.isFetching
      ? spinner
      : <input className={`${Classes.INPUT} ${Classes.FILL}`} onChange={() => {}}type="text" value={this.state.permanentUri} />;
    const embedCode = this.state.isFetching
      ? spinner
      : <textarea className={`${Classes.INPUT} ${Classes.FILL}`} value={this.state.embedCode} onChange={() => {}}></textarea>;
    const source = get(this.props.data, 'data.share.source');
    const disableLatest = !(source && (source !== 'no source'));

    if (this.state.isFetching && !(this.state.isSharing)) return spinner;

    let subtitle = get(this.props, 'meta.subtitle');

    return (
      <div>
        <button
          className={`${Classes.BUTTON} ${Classes.MINIMAL}`}
          onClick={this.onShare}
        >
          <img src={ShareIcon} />
        </button>
        <GDialog isOpen={this.state.isSharing} onClose={this.onClose}>
          <DialogBody>
            <ShareSide>
              <h2>
                { get(this.props.translations, 'share.title', 'Your selection for sharing:') }
              </h2>
              <div>
                <h3>{this.props.meta.title}</h3>
                <h4>
                  {isArray(subtitle) ? subtitle.join(SUBTITLE_SEPARATOR) : subtitle}
                </h4>
              </div>
              <div>
                <RadioGroup onChange={this.onChangeShareMode} selectedValue={this.state.shareMode}>
                  <Radio
                    label={get(this.props.translations, 'share.snapshot', 'Snapshot of data for the period defined (data will not change even if updated on the site)')}
                    value="snapshot"
                  />
                  <Radio
                    label={get(this.props.translations, 'share.latest.available.data', 'Latest available data for the period defined')}
                    value="latest"
                    disabled={disableLatest}
                  />
                </RadioGroup>
              </div>
            </ShareSide>
            <ShareMain>
              <ShareHeader>
                <h2>{get(this.props.translations, 'share.sharing.options', 'Sharing options:')}</h2>
                <Button className={Classes.MINIMAL} onClick={this.onClose} iconName="cross" />
              </ShareHeader>
              <ShareMedia>
                <Button className={Classes.INTENT_PRIMARY} onClick={this.onOutput('facebook')}>Facebook</Button>
                <Button className={Classes.INTENT_PRIMARY} onClick={this.onOutput('twitter')}>Twitter</Button>
                <Button className={Classes.INTENT_PRIMARY} onClick={this.onOutput('email')}>E-Mail</Button>
              </ShareMedia>
              <div>
                <label className={Classes.LABEL}>
                  {get(this.props.translations, 'share.permanent.url', 'Permanent URL')}
                  {permanentUrl}
                  <p className={Classes.TEXT_MUTED}>
                    {get(this.props.translations, 'share.copy.url', 'Copy the URL to open this chart with all your selections.:')}
                  </p>
                </label>
                <label className={Classes.LABEL}>
                  {get(this.props.translations, 'share.embed.code', 'Embed code')}
                  {
                    this.state.hasFormatSelection && (
                      <Select
                        clearable={false}
                        onChange={this.onChangeFormat}
                        options={t4.options}
                        value={this.state.selectedFormat}
                      />
                    )
                  }
                  {embedCode}
                  <p className={Classes.TEXT_MUTED}>
                    {get(this.props.translations, 'share.use.code.to.embed.visualisation', 'Use this code to embed the visualisation into your website.')}
                  </p>
                </label>
                <Button onClick={this.onOutput('preview')}>
                  {get(this.props.translations, 'share.preview.embedded', 'Preview Embedding')}
                </Button>
              </div>
            </ShareMain>
          </DialogBody>
        </GDialog>
      </div>
    );
  }
};

Share.propTypes = {
  config: PropTypes.object,
  data: PropTypes.object,
  meta: PropTypes.object,
  type: PropTypes.string
};

export default Share;
